NEPALI TRANSLATION OF DRUPAL 6.0
================================================================================

This package includes Nepali translation of Drupal core. It's not complete. If you want to contribute please do. Do not forget to inform the Nepali Drupal users community: 
http://groups.google.com/group/drupalnepal

द्रुपलको नेपाली अनुवाद

यदि तपाईँ पनि यो अनुवादमा योगदान गर्न चाहनुहुन्छ भने यहाँलाई हार्दिक स्वागत छ। तपाईँको काम द्रुपलका नेपाली प्रयोगकर्ता/विकासकर्ताहरूको समूहमा पनि जानकारी गराईदिनु होला।
http://groups.google.com/group/drupalnepal


